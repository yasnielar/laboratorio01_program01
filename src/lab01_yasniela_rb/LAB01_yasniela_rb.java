/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab01_yasniela_rb;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author yasni
 */
public class LAB01_yasniela_rb {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion; //Guardaremos la opcion del usuario

        while (!salir) {
            System.out.println("MENÚ DE OPCIONES");
            System.out.println("1. EMPAREJAMIENTO EQUIPOS");
            System.out.println("2. JUEGO DE LOS EQUIPOS");
            System.out.println("3. Salir");
            try {

                System.out.println("Elega una opción: ");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        Emparejamiento.EmparejarVrs();
                        break;
                    case 2:
                        Partido.vs();
                        break;
                    case 3:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 4");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }

    }

}
