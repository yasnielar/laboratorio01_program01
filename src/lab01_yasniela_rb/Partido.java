/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab01_yasniela_rb;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author yasni
 */
public class Partido {

    public static void vs() {

        ArrayList<Integer> listapuntos = new ArrayList<>(); // Lista para los puntos
        listapuntos.add(0);
        listapuntos.add(0);
        listapuntos.add(0);
        listapuntos.add(0);
        listapuntos.add(0);
        listapuntos.add(0);
        //System.out.println(listapuntos);

        Scanner sc = new Scanner(System.in);
        int opcion;
        int Penales;
        int ArribaDerecha = 1;
        int ArribaCentro = 2;
        int ArribaIzquierda = 3;
        int AbajoIzquierda = 4;
        int AbajoCentro = 5;
        int AbajoDerecha = 6;

        Random golesran = new Random();
        int[][] juego = new int[3][2];

        for (int a = 0; a < juego.length; a++) {
            for (int b = 0; b < juego[a].length; b++) {
                //int goles = rand.nextInt() * 5;
                juego[a][b] = golesran.nextInt(6);
            }
        }
        System.out.println();
        System.out.println("|----------------------------------|");
        System.out.println("|Enfrentamiento EQUIPO1 vrs EQUIPO2|");
        System.out.println("|----------------------------------|");
        System.out.println("RESULTADO EQUIPO1: " + juego[0][0] + " goles");
        System.out.println("RESULTADO EQUIPO2: " + juego[0][1] + " goles");

        if ((juego[0][0]) > (juego[0][1])) {
            listapuntos.set(0, 3);
            listapuntos.set(1, 0);
            System.out.println("EL equipo ganador es : " + " EQUIPO1 " + " con " + juego[0][0] + " goles " + "contra" + juego[0][1] + " goles del EQUIPO2");
        } else if ((juego[0][1]) > (juego[0][0])) {
            listapuntos.set(1, 3);
            listapuntos.set(0, 0);
            System.out.println("EL equipo ganador es : " + " EQUIPO2 " + " con " + juego[1][0] + " goles " + "contra" + juego[0][0] + " goles del EQUIPO1");
        } else if (juego[0][0] == juego[0][1]) { //Equipo1 vs Equipo2
            System.out.println("|--------------------------------------------------|");
            System.out.println("|ENFRENTAMIENTO DE PENALES ENTRE EQUPO1 vrs EQUIPO2|");
            System.out.println("|--------------------------------------------------|" + "\n");
            System.out.println("Equipo lanza peneles: " + " EQUIPO1" + "\n");
            Penales = golesran.nextInt(7); //Se excluye el 7.
            //int penal;
            int contador1 = 0;
            int contador2 = 0;
            int contador3 = 0;
            int contador4 = 0;
            int contador5 = 0;
            int contador6 = 0;
            for (int r = 0; r < 5; r++) {
                System.out.println("|=========================|");
                System.out.println("|LANZAMIENTO DE PENALES   |");
                System.out.println("|-------------------------|");
                System.out.println("|1 = Arriba a la derecha  |");
                System.out.println("|2 = Arriba al centro     |");
                System.out.println("|3 = Arriba a la izquierda|");
                System.out.println("|4 = Abajo a la izquierda |");
                System.out.println("|5 = Abajo al centro      |");
                System.out.println("|6 = Arriba a la derecha  |");
                System.out.println("|=========================|" + "\n");
                opcion = sc.nextInt();
                if (opcion != Penales) {
                    System.out.println("\n" + "GOOL" + "\n");
                    contador1++;
                } else if (opcion == Penales) {
                    System.out.println("\n" + "PENAL FALLIDO" + "\n");
                }
                System.out.println("Total de penales del " + " EQUIPO1" + " es igual a: " + contador1 + "\n");
            }
            System.out.println("Equipo lanza peneles: " + " EQUIPO2" + "\n");
            for (int r = 0; r < 5; r++) {
                System.out.println("|=========================|");
                System.out.println("|LANZAMIENTO DE PENALES   |");
                System.out.println("|-------------------------|");
                System.out.println("|1 = Arriba a la derecha  |");
                System.out.println("|2 = Arriba al centro     |");
                System.out.println("|3 = Arriba a la izquierda|");
                System.out.println("|4 = Abajo a la izquierda |");
                System.out.println("|5 = Abajo al centro      |");
                System.out.println("|6 = Arriba a la derecha  |");
                System.out.println("|=========================|");
                opcion = sc.nextInt();
                if (opcion != Penales) {
                    System.out.println("\n" + "GOOL" + "\n");
                    contador2++;
                } else if (opcion == Penales) {
                    System.out.println("\n" + "PENAL FALLIDO" + "\n");
                }
                System.out.println("Total de penales del " + " EQUIPO2" + " es igual a: " + contador2 + "\n");

            }
            if (contador1 > contador2) {
                listapuntos.set(0, 1);
                listapuntos.set(1, 0);
                System.out.println("EL equipo ganador es: " + " EQUIPO1 " + " con " + contador1 + " goles" + " contra " + contador2 + " goles del EQUIPO2");
            } else if (contador1 < contador2) {
                listapuntos.set(1, 1);
                listapuntos.set(0, 0);
                System.out.println("EL equipo ganador es: " + " EQUIPO2 " + " con " + contador2 + " goles de penal" + " contra " + contador1 + " goles de penal del EQUIPO1");
            } else if (contador1 == contador2) {
                listapuntos.set(1, 0);
                listapuntos.set(0, 0);
                System.out.println("EMPATE DE PENALES ENTRE " + " EQUIPO1 " + " Y " + "EQUIPO2");
            }
        }
        System.out.println();
        System.out.println("|----------------------------------|");
        System.out.println("|Enfrentamiento EQUIPO3 vrs EQUIPO4|");
        System.out.println("|----------------------------------|");
        System.out.println("RESULTADO EQUIPO3: " + juego[1][0] + " goles");
        System.out.println("RESULTADO EQUIPO4: " + juego[1][1] + " goles");

        if ((juego[1][0]) > (juego[1][1])) {
            listapuntos.set(2, 3);
            listapuntos.set(3, 0);
            System.out.println("EL equipo ganador es: " + " EQUIPO3 " + " con " + juego[1][0] + " goles " + " contra " + juego[1][1] + " goles del EQUIPO4");
        } else if ((juego[1][1]) > (juego[1][0])) {
            listapuntos.set(3, 3);
            listapuntos.set(2, 0);
            System.out.println("EL equipo ganador es: " + " EQUIPO4 " + " con " + juego[1][1] + " goles " + " contra " + juego[1][0] + " goles del EQUIPO3");
        } else if (juego[1][0] == juego[1][1]) { //Equipo1 vs Equipo2
            System.out.println("|--------------------------------------------------|");
            System.out.println("|ENFRENTAMIENTO DE PENALES ENTRE EQUPO3 vrs EQUIPO4|");
            System.out.println("|--------------------------------------------------|" + "\n");
            System.out.println("Equipo lanza peneles: " + " EQUIPO3" + "\n");
            Penales = golesran.nextInt(7); //Se excluye el 7.
            //int penal;
            int contador1 = 0;
            int contador2 = 0;
            int contador3 = 0;
            int contador4 = 0;
            int contador5 = 0;
            int contador6 = 0;
            for (int r = 0; r < 5; r++) {
                System.out.println("|=========================|");
                System.out.println("|LANZAMIENTO DE PENALES   |");
                System.out.println("|-------------------------|");
                System.out.println("|1 = Arriba a la derecha  |");
                System.out.println("|2 = Arriba al centro     |");
                System.out.println("|3 = Arriba a la izquierda|");
                System.out.println("|4 = Abajo a la izquierda |");
                System.out.println("|5 = Abajo al centro      |");
                System.out.println("|6 = Arriba a la derecha  |");
                System.out.println("|=========================|" + "\n");
                opcion = sc.nextInt();
                if (opcion != Penales) {
                    System.out.println("\n" + "GOOL" + "\n");
                    contador3++;
                } else if (opcion == Penales) {
                    System.out.println("\n" + "PENAL FALLIDO" + "\n");
                }
                System.out.println("Total de penales del " + " EQUIPO3 " + " es igual a: " + contador3 + "\n");
            }
            System.out.println("Equipo lanza peneles: " + " EQUIPO4" + "\n");
            for (int r = 0; r < 5; r++) {
                System.out.println("|=========================|");
                System.out.println("|LANZAMIENTO DE PENALES   |");
                System.out.println("|-------------------------|");
                System.out.println("|1 = Arriba a la derecha  |");
                System.out.println("|2 = Arriba al centro     |");
                System.out.println("|3 = Arriba a la izquierda|");
                System.out.println("|4 = Abajo a la izquierda |");
                System.out.println("|5 = Abajo al centro      |");
                System.out.println("|6 = Arriba a la derecha  |");
                System.out.println("|=========================|");
                opcion = sc.nextInt();
                if (opcion != Penales) {
                    System.out.println("\n" + "GOOL" + "\n");
                    contador4++;
                } else if (opcion == Penales) {
                    System.out.println("\n" + "PENAL FALLIDO" + "\n");
                }
                System.out.println("Total de penales del " + " EQUIPO4 " + " es igual a: " + contador4 + "\n");

            }
            if (contador3 > contador4) {
                listapuntos.set(2, 3);
                listapuntos.set(3, 0);
                System.out.println("EL equipo ganador es: " + " EQUIPO3 " + " con " + contador3 + " goles " + " contra " + contador4 + " goles del EQUIPO4");
            } else if (contador3 < contador4) {
                listapuntos.set(2, 0);
                listapuntos.set(3, 3);
                System.out.println("EL equipo ganador es: " + " EQUIPO4 " + " con " + contador4 + " goles " + " contra " + contador3 + " goles del EQUIPO3");
            } else if (contador3 == contador4) {
                listapuntos.set(2, 0);
                listapuntos.set(3, 0);
                System.out.println("EMPATE DE PENALES ENTRE " + " EQUIPO3 " + " Y " + " EQUIPO4");
            }
        }
        System.out.println();
        System.out.println("|----------------------------------|");
        System.out.println("|Enfrentamiento EQUIPO5 vrs EQUIPO6|");
        System.out.println("|----------------------------------|");
        System.out.println("RESULTADO EQUIPO1:" + juego[2][0] + " goles");
        System.out.println("RESULTADO EQUIPO1:" + juego[2][1] + " goles");

        if ((juego[2][0]) > (juego[2][1])) {
            listapuntos.set(4, 3);
            listapuntos.set(5, 0);
            System.out.println("EL equipo ganador es: " + " EQUIPO5 " + " con " + juego[2][0] + " goles " + " contra " + juego[2][1] + " goles del EQUIPO6");
        } else if ((juego[2][1]) > (juego[2][0])) {
            listapuntos.set(5, 3);
            listapuntos.set(4, 0);
        } else if (juego[2][0] == juego[2][1]) { //Equipo1 vs Equipo2
            System.out.println("|--------------------------------------------------|");
            System.out.println("|ENFRENTAMIENTO DE PENALES ENTRE EQUPO5 vrs EQUIPO6|");
            System.out.println("|--------------------------------------------------|" + "\n");
            System.out.println("Equipo lanza peneles: " + " EQUIPO5" + "\n");
            Penales = golesran.nextInt(7); //Se excluye el 7.
            //int penal;
            int contador1 = 0;
            int contador2 = 0;
            int contador3 = 0;
            int contador4 = 0;
            int contador5 = 0;
            int contador6 = 0;
            for (int r = 0; r < 5; r++) {
                System.out.println("|=========================|");
                System.out.println("|LANZAMIENTO DE PENALES   |");
                System.out.println("|-------------------------|");
                System.out.println("|1 = Arriba a la derecha  |");
                System.out.println("|2 = Arriba al centro     |");
                System.out.println("|3 = Arriba a la izquierda|");
                System.out.println("|4 = Abajo a la izquierda |");
                System.out.println("|5 = Abajo al centro      |");
                System.out.println("|6 = Arriba a la derecha  |");
                System.out.println("|=========================|" + "\n");
                opcion = sc.nextInt();
                if (opcion != Penales) {
                    System.out.println("\n" + "GOOL" + "\n");
                    contador5++;
                } else if (opcion == Penales) {
                    System.out.println("\n" + "PENAL FALLIDO" + "\n");
                }
                System.out.println("Total de penales del " + " EQUIPO1 " + " es igual a: " + contador5 + "\n");
            }
            System.out.println("Equipo lanza peneles: " + " EQUIPO6" + "\n");
            for (int r = 0; r < 5; r++) {
                System.out.println("|=========================|");
                System.out.println("|LANZAMIENTO DE PENALES   |");
                System.out.println("|-------------------------|");
                System.out.println("|1 = Arriba a la derecha  |");
                System.out.println("|2 = Arriba al centro     |");
                System.out.println("|3 = Arriba a la izquierda|");
                System.out.println("|4 = Abajo a la izquierda |");
                System.out.println("|5 = Abajo al centro      |");
                System.out.println("|6 = Arriba a la derecha  |");
                System.out.println("|=========================|");
                opcion = sc.nextInt();
                if (opcion != Penales) {
                    System.out.println("\n" + "GOOL" + "\n");
                    contador6++;
                } else if (opcion == Penales) {
                    System.out.println("\n" + "PENAL FALLIDO" + "\n");
                }
                System.out.println("Total de penales del " + " EQUIPO6 " + " es igual a: " + contador6 + "\n");

            }
            if (contador5 > contador6) {
                listapuntos.set(4, 3);
                listapuntos.set(5, 0);
                System.out.println("EL equipo ganador es: " + " EQUIPO5 " + " con " + contador5 + " goles " + " contra " + contador6 + " goles del EQUIPO6");
            } else if (contador5 < contador6) {
                listapuntos.set(5, 3);
                listapuntos.set(4, 0);
                System.out.println("EL equipo ganador es: " + " EQUIPO6 " + " con " + contador6 + " goles " + " contra " + contador5 + " goles del EQUIPO5");
            } else if (contador5 == contador6) {
                listapuntos.set(4, 0);
                listapuntos.set(5, 0);
                System.out.println("EMPATE DE PENALES ENTRE " + " EQUIPO5 " + " Y " + " EQUIPO6");
            }
        }

        System.out.println("|------------------------------------|");
        System.out.println("|MARCADOR DEL RESULTADO DE LOS JUEGOS|");
        System.out.println("|------------------------------------|");
        for (int a = 0; a < juego.length; a++) {
            for (int b = 0; b < juego[a].length; b++) {
                System.out.print(juego[a][b] + "\t");
            }
            System.out.println();
        }

        int goleseq1, goleseq2, goleseq3, goleseq4, goleseq5, goleseq6; //Goles en el partido
        goleseq1 = juego[0][0];
        goleseq2 = juego[0][1];
        goleseq3 = juego[1][0];
        goleseq4 = juego[1][1];
        goleseq5 = juego[2][0];
        goleseq6 = juego[2][1];
        System.out.println();

        System.out.println("---------RESULTADO PRIMER PARTIDO--------");
        System.out.println("-----------------------------------------");
        System.out.println("*******EQUIPO1 VRS EQUIPO2********");
        System.out.println("GOLES: " + goleseq1 + "\t" + goleseq2);
        System.out.println("PUNTOS: " + listapuntos.get(0) + "\t" + listapuntos.get(1) + "\n");

        System.out.println();
        System.out.println("---------RESULTADO SEGUNDO PARTIDO-------");
        System.out.println("-----------------------------------------");
        System.out.println("********EQUIPO3 VRS EQUIPO4********");
        System.out.println("GOLES: " + goleseq3 + "\t" + goleseq4);
        System.out.println("PUNTOS: " + listapuntos.get(2) + "\t" + listapuntos.get(3) + "\n");

        System.out.println();
        System.out.println("---------RESULTADO TERCER PARTIDO--------");
        System.out.println("-----------------------------------------");
        System.out.println("********EQUIPO5 VRS EQUIPO6********");
        System.out.println("GOLES: " + goleseq5 + "\t" + goleseq6);
        System.out.println("PUNTOS: " + listapuntos.get(4) + "\t" + listapuntos.get(5) + "\n");

        //tabla 
        System.out.println();
        System.out.println("==============================");
        System.out.println("TABLA RELTADOS DE LOS PARTIDOS");
        System.out.println("==============================");

        String[][] encabezado = new String[1][4];
        encabezado[0][0] = "EQUIPOS";
        encabezado[0][1] = "GOLES";
        encabezado[0][2] = "PUNTOS";
        encabezado[0][3] = "PARTIDOS";

        for (int i = 0; i < encabezado.length; i++) { //Se recorre las filas
            for (int j = 0; j < encabezado[i].length; j++) { //Se recorre las matrices
                System.out.print(encabezado[i][j] + "\t");//Se imprime las columnas
            }
            System.out.println();
        }
        System.out.println("==============================");

        int[][] resultadopartido = new int[6][4];

        resultadopartido[0][0] = 1;
        resultadopartido[0][1] = goleseq1;
        resultadopartido[0][2] = listapuntos.get(0);//puntos1;
        resultadopartido[0][3] = 1;
        resultadopartido[1][0] = 2;
        resultadopartido[1][1] = goleseq2;
        resultadopartido[1][2] = listapuntos.get(1);//puntos1;
        resultadopartido[1][3] = 1;
        resultadopartido[2][0] = 3;
        resultadopartido[2][1] = goleseq3;
        resultadopartido[2][2] = listapuntos.get(2);//puntos2;
        resultadopartido[2][3] = 1;
        resultadopartido[3][0] = 4;
        resultadopartido[3][1] = goleseq4;
        resultadopartido[3][2] = listapuntos.get(3);//puntos3;
        resultadopartido[3][3] = 1;
        resultadopartido[4][0] = 5;
        resultadopartido[4][1] = goleseq5;
        resultadopartido[4][2] = listapuntos.get(4);//puntos4;
        resultadopartido[4][3] = 1;
        resultadopartido[5][0] = 6;
        resultadopartido[5][1] = goleseq6;
        resultadopartido[5][2] = listapuntos.get(5);//puntos5;
        resultadopartido[5][3] = 1;

        for (int x = 0; x < resultadopartido.length; x++) {
            for (int y = 0; y < resultadopartido[x].length; y++) {
                System.out.print(resultadopartido[x][y] + "\t");

            }
            System.out.println();
        }
        System.out.println();
    }
}
